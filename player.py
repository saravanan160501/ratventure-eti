class Player:
    def __init__(self, name, damage, defence, hp, coordinate, has_orb):
        self.name = name
        self.damage = damage
        self.defence = defence
        self.hp = hp
        self.coordinate = coordinate
        self.has_orb = has_orb